﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SudokuHelperServices;

namespace Services
{
    public class SudokuService
    {
        //public static Hashtable BoardHashtable = new Hashtable();
        public static string[] Main(string inputBoard)
        {
            //string inputBoard = "003008900000900600008504030802000000010307080000000406060405100004003000009100300";
            //string inputBoard = "390002006050086000200000003030700000001060800000001090400000007000430050800600032";
            //string inputBoard = "000008004084016000000500100103800900608000403002009501007002000000780260200300000";
            //string inputBoard = "000000000000003085001020000000507000004000100090000000500000073002010000000040009";
            var BoardHashtable = new Hashtable();
            var game = new SudokuHelper.Game();
            var brute = true;

            game.BoardHashtable = SudokuHelper.ConvertStringToHashtable(inputBoard, BoardHashtable);
            game.GameEndState = SudokuHelper.IsBoardValid(game.BoardHashtable);
           // var test = SudokuHelper.GameState.InProgress;
            if (game.GameEndState == SudokuHelper.GameState.InProgress) MainProgram(game, game.BoardHashtable);

            if (brute && game.GameEndState == SudokuHelper.GameState.NoProgressMade)
            {
                BruteForceGame(game);
            }

            var boardString = SudokuHelper.ConvertHashtableToString(game.BoardHashtable);

            var returnArray = new string[2] {game.GameEndState.ToString(), boardString};
            return returnArray;

        }

        public static void MainProgram(SudokuHelper.Game g, Hashtable boardHashtable)
        {
            g.GameEndState = SudokuHelper.GameState.InProgress;
            while (g.GameEndState == SudokuHelper.GameState.InProgress)
            {
                var initial = SudokuHelper.CountPossibleValues(boardHashtable);
                for (var i = 0; i < 81; i++)
                {
                    if (g.BoardHashtable[i].ToString().Length != 1) SudokuEliminations.Eliminate(i, boardHashtable);
                }
                var secondary = SudokuHelper.CountPossibleValues(boardHashtable);
                if (secondary < 81) g.GameEndState = SudokuHelper.GameState.Error;
                if (secondary == 81) g.GameEndState = SudokuHelper.GameState.Solved;
                if (initial == secondary) g.GameEndState = SudokuHelper.GameState.NoProgressMade;
            }
        }

        public static void BruteForceGame(SudokuHelper.Game g)
        {
            //Console.WriteLine("Brute force now activated");
            var count = 2;
            foreach (DictionaryEntry piece in g.BoardHashtable)
            {
                if (piece.Value.ToString().Length == count)
                {
                    g.ClonedHashtable = SudokuHelper.CloneHashtable(g.BoardHashtable);

                    var holdValue = piece.Value.ToString();

                    foreach (var piece1 in holdValue)
                    {
                        g.ClonedHashtable[piece.Key] = piece1.ToString();
                        MainProgram(g, g.ClonedHashtable);
                        if (g.GameEndState == SudokuHelper.GameState.Solved)
                        {
                            g.BoardHashtable = g.ClonedHashtable;
                            break;
                        }
                    }
                    if (g.GameEndState == SudokuHelper.GameState.Solved) break;
                    g.ClonedHashtable[piece] = holdValue;
                }
                count++;

                if (count == 10) break;
                if (g.GameEndState == SudokuHelper.GameState.Solved) break;
            }
        }
    }
}
