﻿using System.Configuration;
using Entities.Interfaces;

namespace Services
{
    public class AppSettingsConfigurationProvider : IConfigurationProvider
    {
        public string Get(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }
}
