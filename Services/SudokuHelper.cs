﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace SudokuHelperServices
{
    public class SudokuHelper
    {
        private const string InitialMoves = "123456789";

        public class Game
        {
            public Hashtable BoardHashtable { get; set; } // need to implement this for cleaner code
            public Hashtable ClonedHashtable { get; set; }
            public GameState GameEndState { get; set; }
        }

        public static List<int[]> Regions = new List<int[]>
        {
            new[] {0, 1, 2, 9, 10, 11, 18, 19, 20},
            new[] {3, 4, 5, 12, 13, 14, 21, 22, 23},
            new[] {6, 7, 8, 15, 16, 17, 24, 25, 26},
            new[] {27, 28, 29, 36, 37, 38, 45, 46, 47},
            new[] {30, 31, 32, 39, 40, 41, 48, 49, 50},
            new[] {33, 34, 35, 42, 43, 44, 51, 52, 53},
            new[] {54, 55, 56, 63, 64, 65, 72, 73, 74},
            new[] {57, 58, 59, 66, 67, 68, 75, 76, 77},
            new[] {60, 61, 62, 69, 70, 71, 78, 79, 80}
        };

        public enum GameState
        {
            InProgress = 0,
            NoProgressMade = 1,
            ProgressMade = 2,
            Solved = 3,
            Error = 4,
        }

        public static Hashtable ConvertStringToHashtable(string inputBoard, Hashtable boardHashtable)
        // Converts input to Dictionary
        {
            for (var i = 0; i < 81; i++)
            {
                var character = Int32.Parse(inputBoard.Substring(i, 1));

                boardHashtable.Add(i, character == 0 ? InitialMoves : character.ToString());
            }
            return boardHashtable;
        }

        public static string ConvertHashtableToString(Hashtable boardHashtable)
        {
            var totalString = "";

            for (var i = 0; i < 81; i++)
            {
                totalString += boardHashtable[i].ToString().Length == 1 ? boardHashtable[i].ToString() : "0";
            }
            return totalString;

        }

        public static GameState IsBoardValid(Hashtable boardHashtable)
        {
            if (IsLengthValid(boardHashtable) == GameState.Error) return GameState.Error;
            if (IsColumnsValid(boardHashtable) == GameState.Error) return GameState.Error;
            if (IsRowsValid(boardHashtable) == GameState.Error) return GameState.Error;
            if (IsRegionValid(boardHashtable) == GameState.Error) return GameState.Error;
            return GameState.InProgress;
        }

        private static GameState IsLengthValid(Hashtable boardHashtable)
        {
            if (boardHashtable.Count == 81) return GameState.InProgress;
            return GameState.Error;
        }

        private static GameState IsColumnsValid(Hashtable boardHashtable)
        {
            for (var i = 0; i < 9; i++)
            {
                var colKeys = GetColumnKeys(i);
                var test = CheckValid(boardHashtable, colKeys);

                if (test == false) return GameState.Error;
            }
            return GameState.InProgress;
        }

        private static GameState IsRowsValid(Hashtable boardHashtable)
        {
            for (var i = 0; i < 72; i += 9)
            {
                var rowKeys = GetRowKeys(GetRowNumber(i));
                var test = CheckValid(boardHashtable, rowKeys);

                if (test == false) return GameState.Error;
            }
            return GameState.InProgress;
        }

        private static GameState IsRegionValid(Hashtable boardHashtable)
        {
            foreach (var region in Regions)
            {
                var test = CheckValid(boardHashtable, region);
                if (test == false) return GameState.Error;
            }
            return GameState.InProgress;
        }

        private static bool CheckValid(Hashtable boardHashtable, int[] Keys)
        {
            var list = new List<string>();
            foreach (var key in Keys)
            {
                if (boardHashtable[key].ToString().Length == 1)
                {
                    list.Add(boardHashtable[key].ToString());
                }
            }
            var test1 = list.Count;
            var test2 = list.Distinct().Count();
            return list.Count == list.Distinct().Count();
        }

        public static int[] GetRowKeys(int rowNumber) //Gets the KEY values in a row
        {
            int[] rowKeys = new int[9];
            int arrayNum = 0;
            for (var i = (rowNumber * 9); i < (rowNumber * 9) + 9; i++)
            {
                rowKeys[arrayNum] = i; //Int32.Parse(BoardDictionary[i].ToString());
                arrayNum++;
            }
            return rowKeys;
        }

        public static int[] GetColumnKeys(int colNumber) //Gets the KEY values in a column
        {
            int[] colKeys = new int[9];
            int arrayNum = 0;
            for (var i = colNumber; i < 81; i += 9)
            {
                colKeys[arrayNum] = i; //Int32.Parse(BoardDictionary[i].ToString());
                arrayNum++;
            }
            return colKeys;
        }

        public static int[] GetRegionKeys(int cell)
        {
            return Regions.FirstOrDefault(g => g.Contains(cell));
        }

        public static int GetColumnNumber(int cell) //Gets the column number of a cell
        {
            int c = (cell) % 9;
            return c;
        }

        public static int GetRowNumber(int cell) //gets the row number of a cell
        {
            int offset = (cell) % 9;
            return ((cell - offset) / 9);
        }

        public static int CountPossibleValues(Hashtable boardHashtable)
        {
            var total = "";
            for (var i = 0; i < 81; i++)
            {
                total += boardHashtable[i];
            }

            return total.Length;
        }

        public static void PrintBoard(Hashtable boardHashtable)
        {
            Console.WriteLine("");
            var emptyChar = "_";
            for (var i = 0; i < 81; i += 9)
            {
                Console.WriteLine("{0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8}",
                    boardHashtable[i].ToString().Length == 1 ? boardHashtable[i].ToString() : emptyChar,
                    boardHashtable[i + 1].ToString().Length == 1 ? boardHashtable[i + 1].ToString() : emptyChar,
                    boardHashtable[i + 2].ToString().Length == 1 ? boardHashtable[i + 2].ToString() : emptyChar,
                    boardHashtable[i + 3].ToString().Length == 1 ? boardHashtable[i + 3].ToString() : emptyChar,
                    boardHashtable[i + 4].ToString().Length == 1 ? boardHashtable[i + 4].ToString() : emptyChar,
                    boardHashtable[i + 5].ToString().Length == 1 ? boardHashtable[i + 5].ToString() : emptyChar,
                    boardHashtable[i + 6].ToString().Length == 1 ? boardHashtable[i + 6].ToString() : emptyChar,
                    boardHashtable[i + 7].ToString().Length == 1 ? boardHashtable[i + 7].ToString() : emptyChar,
                    boardHashtable[i + 8].ToString().Length == 1 ? boardHashtable[i + 8].ToString() : emptyChar);
            }
        }

        public static Hashtable CloneHashtable(Hashtable boardHashtable)
        {

            var clonedHashtable = new Hashtable();
            foreach (DictionaryEntry entry in boardHashtable)
            {
                clonedHashtable[entry.Key] = entry.Value;
            }

            return clonedHashtable;
        }


    }
}
