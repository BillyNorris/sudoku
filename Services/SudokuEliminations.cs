﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SudokuHelperServices;

namespace Services
{
    class SudokuEliminations
    {
        public static void Eliminate(int cellIndex, Hashtable boardHashtable)
        {
            EliminateRow(cellIndex, boardHashtable);
            EliminateColumn(cellIndex, boardHashtable);
            EliminateRegion(cellIndex, boardHashtable);

            //get row idxs
            // if solve needed, solve 
            // get cols i
        }


        private static void EliminateRow(int cellIndex, Hashtable boardHashtable)
        {
            var rowPossibleValues = new List<string>();
            var rowKeys = SudokuHelper.GetRowKeys(SudokuHelper.GetRowNumber(cellIndex));
            //var i = 0;
            foreach (var rowidx in rowKeys)
            {
                if (rowidx != cellIndex)
                {
                    rowPossibleValues.Add(boardHashtable[rowidx].ToString());
                }

            }

            InitialElimination(cellIndex, rowPossibleValues.ToArray(), boardHashtable);
        }

        private static void EliminateColumn(int cellIndex, Hashtable boardHashtable)
        {
            var colPossibleValues = new List<string>();
            var colKeys = SudokuHelper.GetColumnKeys(SudokuHelper.GetColumnNumber(cellIndex));
            //var i = 0;
            foreach (var colidx in colKeys)
            {
                if (colidx != cellIndex)
                {
                    colPossibleValues.Add(boardHashtable[colidx].ToString());
                }

            }

            InitialElimination(cellIndex, colPossibleValues.ToArray(), boardHashtable);
        }

        private static void EliminateRegion(int cellIndex, Hashtable boardHashtable)
        {
            var regionPossibleValues = new List<string>();
            var regKeys = SudokuHelper.GetRegionKeys(cellIndex);
            //var i = 0;
            foreach (var regidx in regKeys)
            {
                if (regidx != cellIndex)
                {
                    regionPossibleValues.Add(boardHashtable[regidx].ToString());
                }

            }

            InitialElimination(cellIndex, regionPossibleValues.ToArray(), boardHashtable);
        }


        private static void InitialElimination(int cellIndex, string[] otherCellPossibleValues, Hashtable boardHashtable)
        {

            // for count = X
            // search for all things with a LENGTH of X
            // see if any of the X match
            // if X match, remove all entries of X from other values
            // continue with X until next number
            // when X = 9, break out 
            //var count = 1;
            //while (count < 9)
            for (var count = 1; count < 9; count++)
            {
                //var test = otherCellPossibleValues;
                var query = otherCellPossibleValues.GroupBy(x => x);

                foreach (var query1 in query)
                {
                    if (query1.Key.Length == count && query1.Count() == count)
                    {
                        foreach (var character in query1.Key)
                        {
                            var quickcopy = boardHashtable[cellIndex].ToString();
                            boardHashtable[cellIndex] = quickcopy.Replace(character.ToString(), string.Empty);
                        }
                    }

                    SecondaryElimination(cellIndex, otherCellPossibleValues.ToArray(), boardHashtable);
                }
            }

        }


        public static void SecondaryElimination(int cellIndex, string[] otherCellPossibleValues, Hashtable boardHashtable)
        {

            var query = otherCellPossibleValues;
            //var query1 = boardHashtable[cellIndex].ToString();
            //var count = 0;
            foreach (var possibleValue in boardHashtable[cellIndex].ToString())
            {
                var count = 0;
                foreach (var queries in query)
                {
                    if (queries.Contains(possibleValue))
                    {
                        break;
                    }
                    count++;
                }
                if (count == query.Length)
                {
                    boardHashtable[cellIndex] = possibleValue.ToString();
                }

            }
        }
    }
}
