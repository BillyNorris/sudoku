﻿using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Entities.Extensions;
using Entities.Interfaces;
using Services;
using WebApi.Controllers;

namespace WebApi.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public sealed class SessionAuthorizedAttribute : AuthorizationFilterAttribute
    {
        public override bool AllowMultiple
        {
            get { return false; }
        }

        //public override void OnAuthorization(HttpActionContext actionContext)
        //{
        //    var service = (SecurityService)actionContext.Request.GetDependencyScope().GetService(typeof(ISecurityService));

        //    var sessionId = actionContext.Request.GetAuthToken();
        //    if (!string.IsNullOrEmpty(sessionId))
        //    {
        //        var isSessionValid = service.IsValidSessionId(sessionId);
        //        if (isSessionValid)
        //        {
        //            base.OnAuthorization(actionContext);

        //            var baseController = actionContext.ControllerContext.Controller as AuthApiController;
        //            if (baseController != null) baseController.UserSession = service.GetSession(sessionId);
        //            return;
        //        }
        //    }

            //var nope = new HttpResponseMessage(HttpStatusCode.Unauthorized)
            //{
            //    ReasonPhrase = "You are not authorised"
            //};

            //throw new UnauthorisedException();
            //challengeMessage.Headers.Add("WWW-Authenticate", "Basic");
            //throw new HttpResponseException(nope);
            //actionContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Not authorised");
      //  }
    }

    public class UnauthorisedException : Exception
    {

    }
}