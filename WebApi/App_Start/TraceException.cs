﻿using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Filters;
using WebApi.Attributes;

namespace WebApi
{
    public class TraceExceptionLogger : ExceptionLogger
    {
        public override void Log(ExceptionLoggerContext context)
        {
            Trace.TraceError(context.ExceptionContext.Exception.ToString());
        }
    }

    public class HandleApiExceptionAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            var request = context.ActionContext.Request;

            var response = new
            {
                Message = context.Exception.Message,
                StackTrace = context.Exception.StackTrace
            };

            if (typeof(UnauthorisedException) == context.Exception.GetType())
            {
                context.Response = request.CreateResponse(HttpStatusCode.Unauthorized, response);
            }
            else
            {
                context.Response = request.CreateResponse(HttpStatusCode.BadRequest, response);
            }

        }
    }

}