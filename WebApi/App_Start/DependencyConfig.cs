﻿using System;
using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Entities.Interfaces;
using Services;

namespace WebApi
{
    public static class DependencyConfig
    {
        public static void Init(HttpConfiguration config)
        {
            var builder = new ContainerBuilder();

            var connectionStringName = "MySolutionContext";

            //builder.RegisterType<MySolutionContext>()
            //    .As<IDbContext>()
            //    .WithParameter("connectionString", connectionStringName)
            //    .AsImplementedInterfaces()
            //    .InstancePerRequest();

            builder.RegisterType<AppSettingsConfigurationProvider>().As<IConfigurationProvider>().InstancePerRequest();

            //builder.RegisterGeneric(typeof(BaseRepository<>)).As(typeof(IBaseRepository<>)).InstancePerRequest();
            //builder.RegisterGeneric(typeof(UserBaseRepository<>)).As(typeof(IUserBaseRepository<>)).InstancePerRequest();


            builder.RegisterAssemblyTypes(AppDomain.CurrentDomain.GetAssemblies())
                   .Where(t => t.Name.EndsWith("Service"))
                   .AsImplementedInterfaces()
                   .InstancePerRequest();


            builder.RegisterApiControllers(Assembly.GetExecutingAssembly()).PropertiesAutowired();

            builder.RegisterWebApiFilterProvider(config);

            var container = builder.Build();

            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            //var rulesService = container.Resolve<IRulesService>();
            //rulesService.Seed();
        }
    }
}