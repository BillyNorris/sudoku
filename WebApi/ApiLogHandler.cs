﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;


namespace WebApi
{
    public class ApiLogHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            //var logService = (ILogService)request.GetDependencyScope().GetService(typeof(ILogService));

            var guid = Guid.NewGuid();

            var requestBody = await request.Content.ReadAsStringAsync();

            var path = request.RequestUri.AbsolutePath;

            //if (path.StartsWith("/api/auth"))
            //{
            //    requestBody = "Login Data";
            //}

            //if (!path.StartsWith("/api/logs"))
            //    logService.AddEntry(new LogEntry
            //    {
            //        Path = path,
            //        LinkId = guid,
            //        EntryType = LogEntryType.WebApi,
            //        Description = requestBody,
            //        Reference = "WebApi-Request",
            //    });

            var result = await base.SendAsync(request, cancellationToken);

            string responseBody;

            try
            {
                responseBody = await result.Content.ReadAsStringAsync();
            }
            catch (Exception)
            {
                responseBody = result.StatusCode.ToString();
            }

            //if (!path.StartsWith("/api/logs"))
            //    logService.AddEntry(new LogEntry
            //    {
            //        Path = path,
            //        LinkId = guid,
            //        EntryType = LogEntryType.WebApi,
            //        Description = responseBody,
            //        Reference = "WebApi-Response",
            //    });

            return result;
        }
    }
}