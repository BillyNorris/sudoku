﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Services;

namespace WebApi.Controllers
{

    [RoutePrefix("Sudoku")]
    public class SudokuController : ApiController
    {
        [HttpPost, Route("Solve/{param1}")]
        public IHttpActionResult Solve(string param1)
        {
            //Console.WriteLine("Hello");
            var result = SudokuService.Main(param1);
            return Ok(result);
        }

        [HttpGet, Route("test")]
        public IHttpActionResult Get()
        {
            //Console.WriteLine("Hello");
            //var result = SudokuService.Main(param1);
            //return Ok(result);
            return Ok(true);
        }

    }

}