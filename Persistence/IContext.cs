﻿using System.Data.Entity;
using Entities;

namespace Persistence
{
    public interface IDbContext
    {
        DbSet<LogEntry> Log { get; set; }
        DbSet<User> Users { get; set; }

        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        int SaveChanges();
    }


    public class MySolutionContext : DbContext, IDbContext
    {
        public MySolutionContext()
            : base("MySolutionContext")
        {
        }

        public DbSet<LogEntry> Log { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
