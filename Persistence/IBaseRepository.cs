﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Entities;

namespace Persistence
{
    public interface IBaseRepository<TEntity>
        where TEntity : BaseEntity
    {
        void Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);
        IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate = null);
        IQueryable<TEntity> AsQueryable();
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate = null);
        TEntity FindById(Guid id);
        void Remove(TEntity entity);
        void Remove(Expression<Func<TEntity, bool>> predicate);
        void RemoveRange(IEnumerable<TEntity> entities);
        void SaveChanges();
    }
}