﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Entities;

namespace Persistence
{
    public interface IUserBaseRepository<TEntity>
    where TEntity : UserBaseEntity
    {
        void Add(Guid userId, TEntity entity);
        void AddRange(Guid userId, IEnumerable<TEntity> entities);
        IQueryable<TEntity> Find(Guid userId, Expression<Func<TEntity, bool>> predicate = null);
        IQueryable<TEntity> AsQueryable(Guid userId);
        TEntity FirstOrDefault(Guid userId, Expression<Func<TEntity, bool>> predicate = null);
        TEntity FindById(Guid userId, Guid id);
        void Remove(Guid userId, TEntity entity);
        void Remove(Guid userId, Expression<Func<TEntity, bool>> predicate);
        void RemoveRange(Guid userId, IEnumerable<TEntity> entities);
        void SaveChanges();
    }
}
