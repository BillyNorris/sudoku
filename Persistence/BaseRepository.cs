﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using Entities;

namespace Persistence
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly IDbContext _context;
        private readonly DbSet<TEntity> _entities;

        public BaseRepository(IDbContext context)
        {
            _context = context;
            _entities = context.Set<TEntity>();
        }

        protected DbSet<TEntity> Entities
        {
            get { return _entities; }
        }

        public virtual void Add(TEntity entity)
        {
            _entities.Add(entity);
        }

        public virtual void AddRange(IEnumerable<TEntity> entities)
        {
            _entities.AddRange(entities);
        }

        public virtual IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate = null)
        {
            predicate = predicate ?? (entity => true);

            return _entities.Where(predicate);
        }

        public IQueryable<TEntity> AsQueryable()
        {
            return _entities.AsQueryable();
        }

        public virtual TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate = null)
        {
            predicate = predicate ?? (entity => true);

            return _entities.FirstOrDefault(predicate);
        }

        public virtual TEntity FindById(Guid id)
        {
            return _entities.FirstOrDefault(e => e.Id == id);
        }

        public virtual void Remove(TEntity entity)
        {
            _entities.Remove(entity);
        }

        public virtual void Remove(Expression<Func<TEntity, bool>> predicate)
        {
            var entities = Find(predicate);

            RemoveRange(entities);
        }

        public virtual void RemoveRange(IEnumerable<TEntity> entities)
        {
            _entities.RemoveRange(entities);
        }

        public void SaveChanges()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (OptimisticConcurrencyException oce)
            {
                throw new OptimisticConcurrencyException(
                    "An Entity Framework optimistic concurrency exception occurred.", oce);
            }
            catch (DbUpdateConcurrencyException dbuce)
            {
                throw new OptimisticConcurrencyException(
                    "An Entity Framework optimistic concurrency exception occurred.", dbuce);
            }
        }
    }
}