﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using Entities;

namespace Persistence
{
    public class UserBaseRepository<TEntity> : IUserBaseRepository<TEntity> where TEntity : UserBaseEntity
    {
        private readonly IDbContext _context;
        private readonly DbSet<TEntity> _entities;

        public UserBaseRepository(IDbContext context)
        {
            _context = context;
            _entities = context.Set<TEntity>();
        }

        protected DbSet<TEntity> Entities
        {
            get { return _entities; }
        }

        public virtual void Add(Guid userId, TEntity entity)
        {
            var user = getUser(userId);
            entity.User = user;
            _entities.Add(entity);
        }

        public virtual void AddRange(Guid userId, IEnumerable<TEntity> entities)
        {
            var user = getUser(userId);
            foreach (var entity in entities)
            {
                entity.User = user;
            }
            _entities.AddRange(entities);
        }

        public virtual IQueryable<TEntity> Find(Guid userId, Expression<Func<TEntity, bool>> predicate = null)
        {
            predicate = predicate ?? (entity => true);

            return _entities.Where(i => i.User != null && i.User.Id == userId).Where(predicate);
        }

        public IQueryable<TEntity> AsQueryable(Guid userId)
        {
            return _entities.Where(i => i.User.Id == userId);
        }

        public virtual TEntity FirstOrDefault(Guid userId, Expression<Func<TEntity, bool>> predicate = null)
        {
            predicate = predicate ?? (entity => true);

            return _entities.Where(i => i.User != null && i.User.Id == userId).FirstOrDefault(predicate);
        }

        public virtual TEntity FindById(Guid userId, Guid id)
        {
            return _entities.Where(i => i.User != null && i.User.Id == userId).FirstOrDefault(e => e.Id == id);
        }

        public virtual void Remove(Guid userId, TEntity entity)
        {
            if (entity.User != null && entity.User.Id == userId)
                _entities.Remove(entity);
        }

        public virtual void Remove(Guid userId, Expression<Func<TEntity, bool>> predicate)
        {
            var entities = Find(userId, predicate);

            RemoveRange(userId, entities);
        }

        public virtual void RemoveRange(Guid userId, IEnumerable<TEntity> entities)
        {

            _entities.RemoveRange(entities.Where(i => i.User != null && i.User.Id == userId));
        }

        public void SaveChanges()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (OptimisticConcurrencyException oce)
            {
                throw new OptimisticConcurrencyException(
                    "An Entity Framework optimistic concurrency exception occurred.", oce);
            }
            catch (DbUpdateConcurrencyException dbuce)
            {
                throw new OptimisticConcurrencyException(
                    "An Entity Framework optimistic concurrency exception occurred.", dbuce);
            }
        }

        private User getUser(Guid userId)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == userId);
            if (user == null) throw new Exception("User was not found");
            return user;
        }
    }
}