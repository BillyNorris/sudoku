﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace Entities.Extensions
{
    public static class AuthExtensions
    {
        //public static void AddAuthHeaders(this HttpResponse response, string sessionId, DateTime expiry)
        //{
        //    response.Headers.Add(SpaHeaderType.AccessToken.GetDescription(), sessionId);
        //    response.Headers.Add(SpaHeaderType.TokenExpiry.GetDescription(), expiry.ToUniversalTime().ToString());
        //}

        public static string GetDescription<T>(this T enumerationValue) where T : struct
        {
            var type = enumerationValue.GetType();
            if (!type.IsEnum) throw new ArgumentException("EnumerationValue must be of Enum type", "enumerationValue");

            var memberInfo = type.GetMember(enumerationValue.ToString());
            if (memberInfo.Length <= 0) return enumerationValue.ToString();
            var attrs = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attrs.Length > 0 ? ((DescriptionAttribute)attrs[0]).Description : enumerationValue.ToString();
        }

        public static string GetAuthToken(this HttpRequestMessage request)
        {
            var authToken = request.GetAuthHeaderToken();
            if (!string.IsNullOrEmpty(authToken)) return authToken;
            var accessToken = request.GetAccessToken();
            return string.IsNullOrEmpty(accessToken) ? string.Empty : accessToken;
        }

        private static string GetAuthHeaderToken(this HttpRequestMessage request)
        {
            var authHeader = request.Headers.Authorization;
            if (authHeader == null || string.IsNullOrEmpty(authHeader.Scheme)) return null;
            return authHeader.Scheme;
        }

        private static string GetAccessToken(this HttpRequestMessage request)
        {
            if (!request.Headers.Contains("access-token")) return null;
            var accessTokenHeader = request.Headers.GetValues("access-token");
            return accessTokenHeader == null ? null : accessTokenHeader.FirstOrDefault();
        }
    }
}
