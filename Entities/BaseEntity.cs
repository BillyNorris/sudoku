﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    public interface IBaseEntity
    {
        Guid Id { get; set; }
        DateTime CreatedAt { get; set; }
        byte[] Version { get; set; }
        bool Visible { get; set; }
        DateTime UpdatedAt { get; set; }
    }

    public abstract class BaseEntity : IBaseEntity
    {
        protected BaseEntity()
        {
            CreatedAt = DateTime.UtcNow;
            Version = new byte[] { 1 };
            Visible = true;
            UpdatedAt = DateTime.UtcNow;
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public byte[] Version { get; set; }
        public bool Visible { get; set; }
        public DateTime UpdatedAt { get; set; }
    }

    //public abstract class UserBaseEntity : BaseEntity
    //{
    //    public virtual User User { get; set; }
    //}
}