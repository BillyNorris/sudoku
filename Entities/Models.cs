﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Boards : BaseEntity
    {
        public string inputBoard { get; set; }
        public string outputBoard { get; set; }
    }
}
