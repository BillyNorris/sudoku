﻿namespace Entities.Interfaces
{
    public interface IConfigurationProvider
    {
        string Get(string key);
    }
}
